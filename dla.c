#include <stdio.h>
#include <stdint.h>
#include <tgmath.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include <time.h>

typedef struct {
	int x, y;
} pos_t;

typedef struct {
	size_t width;
	size_t height;
	bool *data;
} lattice_t;

void
update_progress(uint64_t current, uint64_t final)
{
	double progress = (double) current / (double) final;
	int nbars = ceil(progress * 32);
	fprintf(stderr, "\r[");
	for (int i = 0; i < nbars; i++)
		fprintf(stderr, "@");
	for (int i = nbars; i < 32; i++)
		fprintf(stderr, " ");
	fprintf(stderr, "] %4d%%", (int) ceil(progress * 100));
}

int
random_int(int low, int high)
{
	int dx = high - low;
	return (rand() % dx) + low;
}

void
step(const lattice_t lattice, pos_t *restrict pos)
{
	if (random_int(0,2))
		pos->x += random_int(-1,2);
	else
		pos->y += random_int(-1,2);

	if (pos->x < 0) pos->x = 0;
	if (pos->y < 0) pos->y = 0;
	if (pos->x >= lattice.width) pos->x = lattice.width - 1;
	if (pos->y >= lattice.height) pos->y = lattice.height - 1;
}

bool
empty_neighbours(const lattice_t lattice, pos_t *restrict pos)
{
	if (pos->x < lattice.width
	    && lattice.data[(pos->x+1) + pos->y * lattice.width])
		return false;
	if (pos->x > 0
	    && lattice.data[pos->x-1 + pos->y * lattice.width])
		return false;
	if (pos->y < lattice.height
	    && lattice.data[pos->x + (pos->y+1) * lattice.width])
		return false;
	if (pos->y > 0
	    && lattice.data[pos->x + (pos->y-1) * lattice.width])
		return false;
	return true;
}

double
rand_real(void)
{
	return ((double) rand()) / ((double) RAND_MAX);
}

void
iterate(const lattice_t lattice, pos_t *restrict pos, double stick_coeff)
{
	do step(lattice, pos);
	while (empty_neighbours(lattice, pos) || rand_real() > stick_coeff);
	lattice.data[pos->x + pos->y * lattice.width] = true;
}

void
run_dla(const lattice_t lattice, pos_t *restrict pos, int N, double prob)
{
	int x, y, z;
	for (int i = 0; i < N; i++) {
		z = random_int(0, 4);
		x = random_int(0, lattice.width);
		y = random_int(0, lattice.height);
		switch (z) {
		case 0: pos->x = 0;
			pos->y = y;
			break;
		case 1: pos->x = lattice.width - 1;
			pos->y = y;
			break;
		case 2: pos->x = x;
			pos->y = 0;
			break;
		case 3: pos->x = x;
			pos->y = lattice.height - 1;
			break;
		}
		if ((i % 100) == 0) {
			update_progress(i, N);
		}
		iterate(lattice, pos, prob);
	}
	update_progress(N, N);
	fprintf(stderr, "\n");
}

void
export_netpbm(const lattice_t* restrict lattice)
{
	printf("P1\n%zu %zu\n", lattice->width, lattice->height);
	for (int j = 0; j < lattice->height; j++) {
		for (int i = 0; i < lattice->width; i++)
			printf("%d ", (int) lattice->data[i + j*lattice->width]);
		printf("\n");
	}
}

#define WIDTH 256
#define HEIGHT 256

int
main(int argc, char *argv[])
{
	lattice_t lattice = {
		.width = WIDTH,
		.height = HEIGHT,
		.data = NULL
	};
	pos_t pos = {0,0};
	double prob = 1.0;
	int n = 6144;

	srand(time(NULL));

	if (argc >= 5) {
		sscanf(argv[1], "%zu", &lattice.width);
		sscanf(argv[2], "%zu", &lattice.height);
		sscanf(argv[3], "%d", &n);
		sscanf(argv[4], "%lf", &prob);
	}

	lattice.data = calloc(lattice.width * lattice.height, sizeof(bool));
	lattice.data[lattice.width/2 + (lattice.width * lattice.height)/2] = true;

	run_dla(lattice, &pos, n, prob);
	export_netpbm(&lattice);
	return 0;
}
