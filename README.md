# About

The program contained in this repository performs a random walk to generate
DLA fractal.

# Compilation

    make dla
    
# Usage

The default options are to create an image with width and height 256 pixels,
using 8192 points in the random walk and a sticking probability of 1.0.

    ./dla > generated-image.pbm

These options can be specified explicity as follows:

    ./dla [WIDTH] [HEIGHT] [NUMBER-OF-POINTS] [STICKING-PROBABILITY] > ./generated-image.pbm
    
# License

The code is released under the following license,

> Copyright © 2017 Joseph Davis <davisj2@tcd.ie>
>
> Permission to use, copy, modify, and distribute this software for any
> purpose with or without fee is hereby granted, provided that the above
> copyright notice and this permission notice appear in all copies.
>
> THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
> WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
> MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
> ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
> WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
> ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
> OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
