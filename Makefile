CFLAGS = -O3 -march=native -std=gnu99

all: dla

dla: dla.c
	${CC} ${CFLAGS} -o dla dla.c

clean:
	rm dla
